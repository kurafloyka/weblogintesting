import core.BrowserNames;
import core.SingletonBrowserClass;
import element.ReadFiles;
import operations.*;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class SeleniumTestCasesWithSingleton {


    public WebDriver driver;
    private static Logger LOGGER;
    SingletonBrowserClass sbc;


    @BeforeClass
    public static void setupClass() {
        LOGGER = LoggerFactory.getLogger(SeleniumTestCasesWithSingleton.class);
    }

    @Before
    public void setupTest() {
        sbc = SingletonBrowserClass.getInstanceOfSingletonBrowserClass(BrowserNames.CHROME);
        driver = sbc.getDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        LOGGER.info("CHROME IS OPENED...");
        driver.get("http://automationpractice.com/index.php");
    }


    @Test
    public void signInWithNoCredentials() {

        ClickOperation clickOperation = new ClickOperation(driver);
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("signInButton"));

        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);
        sendKeysOperation.sendKeys(ReadFiles.readLocator("emailField"), "");
        sendKeysOperation.sendKeys(ReadFiles.readLocator("passwordField"), "");
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("loginButton"));


        AdditionalOperation additionalOperation = new AdditionalOperation(driver);

        LOGGER.info(additionalOperation.getText(ReadFiles.readLocator("errorMessage")));
        AssertionOperation assertionOperation = new AssertionOperation();
        assertionOperation.checkEquals("An email address required.", additionalOperation.getText(ReadFiles.readLocator("errorMessage")));


    }

    @Test
    public void signInWithEmailAndEmptyPassword() {

        ClickOperation clickOperation = new ClickOperation(driver);
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("signInButton"));

        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);

        sendKeysOperation.sendKeys(ReadFiles.readLocator("emailField"), "farukakyol@gmail.com");
        sendKeysOperation.sendKeys(ReadFiles.readLocator("passwordField"), "");
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("loginButton"));


        AdditionalOperation additionalOperation = new AdditionalOperation(driver);

//LOGGER.info(additionalOperation.getText(ReadFiles.readLocator("errorMessage")));
        AssertionOperation assertionOperation = new AssertionOperation();
        assertionOperation.checkEquals("Password is required.", additionalOperation.getText(ReadFiles.readLocator("errorMessage")));


    }

    @Test
    public void signInWithPasswordAndEmptyEmail() {

        ClickOperation clickOperation = new ClickOperation(driver);
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("signInButton"));

        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);
        sendKeysOperation.sendKeys(ReadFiles.readLocator("emailField"), "");
        sendKeysOperation.sendKeys(ReadFiles.readLocator("passwordField"), "123456");
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("loginButton"));


        AdditionalOperation additionalOperation = new AdditionalOperation(driver);
        LOGGER.info(additionalOperation.getText(ReadFiles.readLocator("errorMessage")));

        AssertionOperation assertionOperation = new AssertionOperation();
        assertionOperation.checkEquals("An email address required.", additionalOperation.getText(ReadFiles.readLocator("errorMessage")));
    }

    @Test
    public void signInWithIncorrectCreditials() {

        ClickOperation clickOperation = new ClickOperation(driver);
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("signInButton"));

        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);
        sendKeysOperation.sendKeys(ReadFiles.readLocator("emailField"), "farukakyol@gmail.com");
        sendKeysOperation.sendKeys(ReadFiles.readLocator("passwordField"), "123456");
        clickOperation.clickWithJavaScript(ReadFiles.readLocator("loginButton"));


        AdditionalOperation additionalOperation = new AdditionalOperation(driver);


        AssertionOperation assertionOperation = new AssertionOperation();
        assertionOperation.checkEquals("Authentication failed.", additionalOperation.getText(ReadFiles.readLocator("errorMessage")));
    }

    /*@After
    public void tearDown() {
        driver.close();
        driver.quit();
        LOGGER.info("BROWSER IS CLOSED...");
    }*/


}



