package core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SingletonBrowserClass {

    // instance of singleton class
    private static SingletonBrowserClass instanceOfSingletonBrowserClass = null;


    private WebDriver driver;
    private BrowserNames browserNames;
    private static Logger LOGGER = LoggerFactory.getLogger(SingletonBrowserClass.class);

    private ChromeOptions options;
    private DesiredCapabilities desiredCapabilities;


    // Constructor
    public SingletonBrowserClass(BrowserNames browserNames) {
        this.browserNames = browserNames;

        if (browserNames == BrowserNames.CHROME) {

            WebDriverManager.chromedriver().setup();


            options = new ChromeOptions();
            desiredCapabilities = DesiredCapabilities.chrome();


            options.addArguments("--test-type");
            options.addArguments("--disable-popup-blocking");
            options.addArguments("--disable-headless");
            options.addArguments("--disable-notifications");
            options.addArguments("--start-maximized");
            options.addArguments("--lang=tr");
            //options.setExperimentalOption("useAutomationExtension", true);
            Map<String, Object> prefs = new HashMap<>();
            prefs.put("browser.helperApps.neverAsk.saveToDisk", "text/csv");
            prefs.put("download.default_directory", "\\\\backupsrv\\Calisma_Gruplari\\OZEL BIRIMLER\\AZTR TEST\\PDF");
            options.setExperimentalOption("prefs", prefs);

            desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);

            desiredCapabilities.setBrowserName(options.getBrowserName().toUpperCase());
            desiredCapabilities.setVersion(System.getProperty("os.version"));
            desiredCapabilities.setPlatform(Platform.getCurrent());
            desiredCapabilities.setCapability("acceptSslCerts", "true");

            driver = new ChromeDriver(options);
            LOGGER.info("CHROME IS STARTING...");


        } else if (browserNames == BrowserNames.FIREFOX) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            LOGGER.info("FIREFOX IS STARTING...");
        }


    }

    // TO create instance of class
    public static SingletonBrowserClass getInstanceOfSingletonBrowserClass(BrowserNames browserNames1) {
        if (instanceOfSingletonBrowserClass == null) {
            instanceOfSingletonBrowserClass = new SingletonBrowserClass(browserNames1);
        }
        return instanceOfSingletonBrowserClass;
    }

    // To get driver
    public WebDriver getDriver() {


        if (driver == null) {
            String errorMessage = "Driver null durumda!";
            LOGGER.info(errorMessage);
        }

        return driver;
    }


    public static void main(String[] args) {

        SingletonBrowserClass sbc1 = SingletonBrowserClass.getInstanceOfSingletonBrowserClass(BrowserNames.CHROME);
        WebDriver driver1 = sbc1.getDriver();
        driver1.get("https://www.google.com");
        driver1.quit();


    }


}