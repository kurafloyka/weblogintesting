package operations;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ScrollOperation {

    private WebDriver driver;
    private WaitOperation waitOperation;

    public ScrollOperation(WebDriver driver) {

        this.driver = driver;
        this.waitOperation = new WaitOperation(driver);
    }


    public void scrollToElement(By by) {

        WebElement webElement = waitOperation.waitPresence(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded();", webElement);
    }
}

