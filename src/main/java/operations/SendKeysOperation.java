package operations;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendKeysOperation {

    private Logger log = LoggerFactory.getLogger(SendKeysOperation.class);

    private WaitOperation waitOperation;
    private WebDriver driver;
    private ScrollOperation scrollOperation;

    public SendKeysOperation(WebDriver driver) {

        this.waitOperation = new WaitOperation(driver);
        this.driver = driver;
        this.scrollOperation = new ScrollOperation(driver);
    }

    public void sendKeys(By locator, String value) {

        try {
            waitOperation.waitPresence(locator);
            waitOperation.waitVisible(locator);
            scrollOperation.scrollToElement(locator);
            WebElement webElement = waitOperation.waitClickable(locator);
            webElement.sendKeys(value);
        } catch (StaleElementReferenceException | ElementNotInteractableException staleElementReferenceException) {
            String logMessage = String.format("'%s' elementine sendKey yaparken sorun oluştu!", locator);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            sendKeys(locator, value);
        }
    }

    public void sendKeyWithJavaScript(By locator, String value) {
        try {
            waitOperation.waitPresence(locator);
            scrollOperation.scrollToElement(locator);
            WebElement webElement = waitOperation.waitVisible(locator);
            String injectScript = String.format("arguments[0].value='%s';", value);
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript(injectScript, webElement);
        } catch (StaleElementReferenceException | ElementNotInteractableException staleElementReferenceException) {
            String logMessage = String.format("'%s' elementine sendKey yaparken sorun oluştu!", locator);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            sendKeys(locator, value);
        }
    }

    public void clear(By locator) {

        try {
            waitOperation.waitPresence(locator);
            waitOperation.waitVisible(locator);
            scrollOperation.scrollToElement(locator);
            waitOperation.waitUntilReadyForDocumentObjectModel();
            WebElement webElement = waitOperation.waitClickable(locator);
            webElement.clear();
        } catch (StaleElementReferenceException staleElementReferenceException) {
            String logMessage = String.format("'%s' elementi temizlenirken sorun oluştu!", locator);
            log.error(logMessage);
            WaitOperation.waitDoNotWriteToLogFile(1);
            clear(locator);
        } catch (Exception exception) {
            String exceptionMessage = String.format("'%s' objesinin text değeri temizlenirken hata oluştu! Hata kodu: %s", locator, exception.getMessage());
            log.error(exceptionMessage);
            Assert.fail(exceptionMessage);
        }
    }
}
