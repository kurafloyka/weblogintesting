package operations;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileOperation {

    private static File file;
    private static Logger log = LoggerFactory.getLogger(FileOperation.class);

    private FileOperation() { }

    public static boolean remove(String filePath, String fileName) {
        file = new File(filePath + fileName);
        return file.exists() && file.delete();
    }

    public static boolean isExists(String filePath, String fileName) {
        file = new File(filePath + fileName);
        return file.exists();
    }

    public static boolean isEmpty(String filePath, String fileName) {
        file = new File(filePath + fileName);
        return file.length() <= 0;
    }

    public static String readFileAsString(String fileName) {
        String data = "";
        try {
            data = new String(Files.readAllBytes(Paths.get(fileName)));
        }
        catch (IOException e) {
            log.error(e.getMessage());
        }
        return data;
    }
}
